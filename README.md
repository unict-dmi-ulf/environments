# Use cases

## Setup a test environment

Update and launch a test compose configuration:

```
docker-compose -f test.yml up
```

Fill this instance with test data loading the collection 'test-environment-setup' over [Thunder](https://www.thunderclient.io/) and run the whole collection with 'Run all'.

The test configuration uses an in-memory database, so data are erased on down.
